# Installing ***mod_perl*** & ***enabling:***
```bash
zypper -n in apache2-mod_perl
a2enmod mod_perl
rcapache2 restart
```

&#x1F4D8; 1 
# The module "CurrentTime.pm" must by saved in Perl modules directory ("@INC - built-in array perl provides. It contains a series of directories, the "search path" for perl when trying to load a module") which Perl using it, to communicate with apache.

  In my case, I write this module in::
```
/usr/lib/perl5/vendor_perl/5.26.1/x86_64-linux-thread-multi/Apache2
```
 # NOTE: I Using SUSELeep 15 and Apache/2.4.29 (Linux/SUSE) for this test.

---------------------------------------------------------------------

&#x1F4D8; 2

# Configuring Apache to respond from your new module :)
  Going to:
```
vim /etc/apache2/httpd.conf
```
- And add "Response Headers" which Apache must use for recognizing your module when Perl giving request.
```
PerlModule Apache2::CurrentTime
<Location /time>
      SetHandler modperl
      PerlOptions +GlobalRequest
      PerlResponseHandler Apache2::CurrentTime
</Location>

```
&#x1F4D8; 3

# Ok, Restart your apache, after then going and test it on your browser:

```
http://localhost/time/
```

# NOTE:
# What are Handlers?

- Apache distinguishes between numerous phases for which it provides hooks (because the C functions are called ap_hook_<phase_name>) where modules can plug various callbacks to extend and alter the default behavior of the webserver. mod_perl provides a Perl interface for most of the available hooks, so mod_perl modules writers can change the Apache behavior in Perl. These callbacks are usually referred to as handlers and therefore the configuration directives for the mod_perl handlers look like: PerlFooHandler, where Foo is one of the handler names. For example PerlResponseHandler configures the response callback.


# Good luck, friends ;)









