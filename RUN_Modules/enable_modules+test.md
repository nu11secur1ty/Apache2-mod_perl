# Enable and disable CGI modules
```bash
a2enmod mod_alias
a2enmod mod_cgi
a2enmod mod_cgid
a2enmod mod_perl
----------------
a2dismod mod_alias
a2dismod mod_cgi
a2dismod mod_cgid
a2dismod mod_perl
```
# The CGI directory ***NOTE:*** This is on OpenSuse Leap15 Linux
```bash
cd /srv/www/cgi-bin
```
- add your first program
```perl
#!/usr/bin/perl  
use strict;
use warnings;
use diagnostics;
use CGI; 

  my $cgi = new CGI; 
    print $cgi->header; 
      print $cgi->start_html('Hello CGI'); 
      print $cgi->h1('Hello CGI'); 
      print $cgi->end_html(); 
exit; 
```
# Restart your apache
```bash 
rcapache2 restart
```
# Have fun :)
