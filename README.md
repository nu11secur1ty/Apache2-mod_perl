# Apache2-mod_perl
# How to Write a module and test it, using mod_perl.

----------------------------------------------------------------------
# Installing ```mod_perl```For openSUSE Leap 15.0 run the following as root:

```bash
zypper addrepo https://download.opensuse.org/repositories/devel:languages:perl/openSUSE_Leap_15.0/devel:languages:perl.repo
zypper refresh
zypper install apache2-mod_perl
```
# Installing ```mod_perl```For openSUSE Leap 42.3, 42.2 run the following as root:
```bash
zypper addrepo https://download.opensuse.org/repositories/Apache:Modules/openSUSE_Leap_42.3/Apache:Modules.repo
zypper refresh
zypper install apache2-mod_perl
```
# iNSTALLING:

***mod_perl*** **enables you to run Perl scripts in an embedded interpreter. The persistent interpreter embedded in the server avoids the overhead of starting an external interpreter and the penalty of Perl start-up time.
    Package Name: ***apache2-mod_perl***
    Configuration File: ```/etc/apache2/conf.d/mod_perl.conf```
    More Information: ```/usr/share/doc/packages/apache2-mod_perl``` 


----------------------------------------------------------------------------------------------------



&#x1F4D8; 1 
# The module "CurrentTime.pm" must by saved in Perl modules directory ("@INC - built-in array perl provides. It contains a series of directories, the "search path" for perl when trying to load a module") which Perl using it, to communicate with apache.


In my case, I write this module "CurrentTime.pm" in::

```
/usr/lib/perl5/vendor_perl/5.26.1/x86_64-linux-thread-multi/Apache2
```
# For Leep 42.3 the dir is:
```bash
/usr/lib/perl5/vendor_perl/5.18.2/x86_64-linux-thread-multi/Apache2/
```
 
 # NOTE: I Using SUSELeep 15 and Apache/2.4.29 (Linux/SUSE) for this test.

---------------------------------------------------------------------

&#x1F4D8; 2

# Configuring Apache to respond from your new module :)
 - Going to:

```
vim /etc/apache2/httpd.conf
```
- And add "Response Headers" which Apache must use for recognizing your module when Perl giving request.

```
# Current Time
PerlModule Apache2::CurrentTime
<Location /time>
      SetHandler modperl
      PerlOptions +GlobalRequest
      PerlResponseHandler Apache2::CurrentTime
</Location>

```
- Change to directory: ```/usr/lib/perl5/vendor_perl/5.18.2/x86_64-linux-thread-multi/Apache2/``` and then paste all code from ```CurrentTime.pm``` file which you can find in **Modules** dir on this repository.

&#x1F4D8; 3

# Ok, Restart your apache, after then going and test it on your browser:

```
http://localhost/time/
```

# Good luck, friends ;)











