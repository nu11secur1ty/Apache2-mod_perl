# You need to execute this over a Web Server in to your ***/cgi-bin/*** directory!
- In my case:
```bash
/srv/www/cgi-bin
```
# Apache **httpd** conf
- Add in to ***httpd.conf***
```
################## mod_perl ################################################
# Enable .pl
<Files ~ "\.pl$">
  Options +ExecCGI
</Files>
```
# Give an execution permissions
```bash
chmod a+xr hello.pl
```
