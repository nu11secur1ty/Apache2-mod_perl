# Create module pm:
- Add in CurrentTime.pm which must be located in```/usr/lib/perl5/vendor_perl/5.18.2/x86_64-linux-thread-multi/Apache2``` for Perl 5.18.2 for Leap 15.01```/usr/lib/perl5/vendor_perl/5.26.1/x86_64-linux-thread-multi/Apache2/```

```bash
package Apache2::CurrentTime;
  
use strict;
use warnings;
  
use Apache2::RequestRec ();
use Apache2::RequestIO ();
  
use Apache2::Const -compile => qw(OK);
  
sub handler {
    my $r = shift;
  
    $r->content_type('text/plain');
    $r->print("Now is: " . scalar(localtime) . "\n");
  
    return Apache2::Const::OK;
}
1;
```


# Add in httpd.conf
```bash
# Current Time:
PerlModule Apache2::CurrentTime
<Location /time>
      SetHandler modperl
      PerlOptions +GlobalRequest
      PerlResponseHandler Apache2::CurrentTime
</Location>
```

# restart apache2:
```bash
rcapache2 restart
```


# Have fun ;)
