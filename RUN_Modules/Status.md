# Enabling dependent modules:
```bash
a2enmod mod_info
a2enmod mod_status
a2enmod mod_perl
```
# Add in httpd.conf
```bash
# Status of the server-perl-status
<Location /perl-status>
      SetHandler modperl
      PerlOptions +GlobalRequest
      PerlResponseHandler Apache2::Status
</Location>
```
# Test 
```url
http://localhost/perl-status
```
