There are many more places in our lives where filters are used. The purpose of all filters is to apply some transformation to what's coming into the filter, letting something different out of the filter. Certainly in some cases it's possible to modify the source itself, but that makes things unflexible, and but most of the time we have no control over the source. The advantage of using filters to modify something is that they can be replaced when requirements change Filters also can be stacked, which allows us to make each filter do simple transformations. For example by combining several different filters, we can apply multiple transformations. In certain situations combining several filters of the same kind let's us achieve a better quality output.

The mod_perl filters are not any different, they receive some data, modify it and send it out. In the case of filtering the output of the response handler, we could certainly change the response handler's logic to do something different, since we control the response handler. But this may make the code unnecessary complex. If we can apply transformations to the response handler's output, it certainly gives us more flexibility and simplifies things. For example if a response needs to be compressed before sent out, it'd be very inconvenient and inefficient to code in the response handler itself. Using a filter for that purpose is a perfect solution. Similarly, in certain cases, using an input filter to transform the incoming request data is the most wise solution. Think of the same example of having the incoming data coming compressed.

Just like with real life filters, you can pipe several filters to modify each other's output. You can also customize a selection of different filters at run time.

Without much further ado, let's write a simple but useful obfuscation filter for our HTML documents.

We are going to use a very simple obfuscation -- turn an HTML document into a one liner, which will make it harder to read its source without a special processing. To accomplish that we are going to remove characters \012 (\n) and \015 (\r), which depending on the platform alone or as a combination represent the end of line and a carriage return.

And here is the filter handler code:

```
  #file:MyApache2/FilterObfuscate.pm
  #--------------------------------
  package MyApache2::FilterObfuscate;
  
  use strict;
  use warnings;
  
  use Apache2::Filter ();
  use Apache2::RequestRec ();
  use APR::Table ();
  
  use Apache2::Const -compile => qw(OK);
  
  use constant BUFF_LEN => 1024;
  
  sub handler {
      my $f = shift;
  
      unless ($f->ctx) {
          $f->r->headers_out->unset('Content-Length');
          $f->ctx(1);
      }
  
      while ($f->read(my $buffer, BUFF_LEN)) {
          $buffer =~ s/[\r\n]//g;
          $f->print($buffer);
      }
  
      return Apache2::Const::OK;
  }
  1;
  ```
  
# The directives below configure Apache to apply the MyApache2::FilterObfuscate filter to all requests that get mapped to files with an ".html" extension:
  
```
  <Files ~ "\.html">
      PerlOutputFilterHandler MyApache2::FilterObfuscate
  </Files>
```
Filters are expected to return Apache2::Const::OK or Apache2::Const::DECLINED. But instead of receiving $r (the request object) as the first argument, they receive $f (the filter object). The filter object is described later in this chapter.

The filter starts by unsetting the Content-Length response header, because it modifies the length of the response body (shrinks it). If the response handler sets the Content-Length header and the filter doesn't unset it, the client may have problems receiving the response since it will expect more data than it was sent. Setting the Content-Length Header below describes how to set the Content-Length header if you need to.

The core of this filter is a read-modify-print expression in a while loop. The logic is very simple: read at most BUFF_LEN characters of data into $buffer, apply the regex to remove any occurences of \n and \r in it, and print the resulting data out. The input data may come from a response handler, or from an upstream filter. The output data goes to the next filter in the output chain. Even though in this example we haven't configured any more filters, internally Apache itself uses several core filters to manipulate the data and send it out to the client.

As we are going to explain in detail in the following sections, the same filter may be called many times during a single request, every time receiving a subsequent chunk of data. For example if the POSTed request data is 64k long, an input filter could be invoked 8 times, each time receiving 8k of data. The same may happen during the response phase, where an upstream filter may split 64k of output in 8, 8k chunks. The while loop that we just saw is going to read each of these 8k in 8 calls, since it requests 1k on every read() call.

Since it's enough to unset the Content-Length header when the filter is called the first time, we need to have some flag telling us whether we have done the job. The method ctx() provides this functionality:

```
      unless ($f->ctx) {
          $f->r->headers_out->unset('Content-Length');
          $f->ctx(1);
      }
```

The unset() call will be made only on the first filter call for each request. You can store any kind of a Perl data structure in $f->ctx and retrieve it later in subsequent filter invocations of the same request. There are several examples using this method in the following sections.

To be truly useful, the MyApache2::FilterObfuscate filter logic should take into account situations where removing new line characters will make the document render incorrectly in the browser. As we mentioned above, this is the case if there are multi-line... 
```
<pre>...</pre> 
```
entries. Since this increases the complexity of the filter, we will disregard this requirement for now.
A positive side-effect of this obfuscation algorithm is that it reduces the amount of the data sent to the client. The Apache::Clean module, available from the CPAN, provides a production-ready implementation of this technique which takes into account the HTML markup specifics.

mod_perl I/O filtering follows the Perl principle of making simple things easy and difficult things possible. You have seen that it's trivial to write simple filters. As you read through this chapter you will see that much more difficult things are possible, and that the code is more elaborate.



